package net.scalytica.sbt.gitlab.changelog

import net.scalytica.sbt.gitlab.test.{IntegrationConfigs, ModelFixtures}
import org.scalatest.Inspectors.forAll
import org.scalatest.{MustMatchers, OptionValues, WordSpec}

// scalastyle:off magic.number
class GitLabChangeLogProcessorSpec
    extends WordSpec
    with MustMatchers
    with OptionValues
    with ModelFixtures
    with IntegrationConfigs {

  val processor =
    GitLabChangeLogProcessor(
      gitlabHost = host,
      accessToken = token,
      projectPath = projectUri,
      destPath = testDestPath,
      startAtTag = Some("v1.1.0")
    )

  "The changelog processor" should {

    "return a ChangeLog" in {
      val res = processor.changeLog.unsafeRunSync()
      res.entries must not be empty

      // Just check a few entries
      forAll(res.entries.takeRight(5))(e => e.commits must not be empty)
    }

  }

}
