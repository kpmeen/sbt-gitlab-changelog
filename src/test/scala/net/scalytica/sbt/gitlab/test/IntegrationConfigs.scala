package net.scalytica.sbt.gitlab.test

import java.nio.file.Paths

import net.scalytica.sbt.gitlab.api.{AccessToken, GitLabHost, ProjectUri}

trait IntegrationConfigs {

  private[this] val tokenKey = "GITLAB_API_TOKEN"

  val host = GitLabHost()

  val projectUri = ProjectUri("kpmeen", "clammyscan")

  val token = AccessToken(sys.props.get(tokenKey).getOrElse(sys.env(tokenKey)))

  val testDestPath = Paths.get("target/TEST-CHANGELOG.md")

}
