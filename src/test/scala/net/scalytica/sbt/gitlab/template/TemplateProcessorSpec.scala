package net.scalytica.sbt.gitlab.template

import java.nio.file.Files

import net.scalytica.sbt.gitlab.api.ProjectUri
import net.scalytica.sbt.gitlab.template.TemplateContextKeys._
import net.scalytica.sbt.gitlab.test.{IntegrationConfigs, ModelFixtures}
import org.scalatest._

import scala.io.{Codec, Source}

class TemplateProcessorSpec
    extends WordSpec
    with MustMatchers
    with TryValues
    with OptionValues
    with BeforeAndAfterEach
    with ModelFixtures
    with IntegrationConfigs {

  val testProjectUri = ProjectUri("test", "test-project")

  val defaultProcessor = new TemplateProcessor(
    projectUri = testProjectUri,
    changeLogPath = testDestPath
  )

  override def afterEach(): Unit = {
    Files.deleteIfExists(testDestPath)
    super.afterEach()
  }

  private[this] def validateContent(content: String): Assertion = {
    content must startWith("# Changelog for test-project")
    content must endWith(
      s"""<sub>Created using [sbt-gitlab-changelog]""" +
        """(https://gitlab.com/kpmeen/sbt-gitlab-changelog).</sub>"""
    )
    content.split("\n").length mustBe 34
  }

  "The template processor" should {

    "produce a template context Map from a ChangeLog" in {
      val cl = changeLog()

      val res = defaultProcessor.changeLogContext(cl)

      res must have size 3
      res.get(TitleKey) mustBe Some("Changelog for test-project")

      val entriesAny = res.get(ChangeLogEntriesKey).value
      entriesAny mustBe a[Seq[_]]
      val entries = entriesAny.asInstanceOf[Seq[Map[String, Any]]]
      entries must have size 5
    }

    "render a markdown String from a ChangeLog" in {
      val cl = changeLog()

      val res = defaultProcessor.renderChangelogString(cl).unsafeRunSync()
      validateContent(res)
    }

    "render a CHANGELOG.md file from a ChangeLog" in {
      val cl = changeLog()

      defaultProcessor.renderChangelogFile(cl).unsafeRunSync()

      Files.exists(testDestPath) mustBe true
      val src = Source.fromFile(testDestPath.toUri)(Codec.UTF8).mkString("")
      validateContent(src)
    }

  }

}
