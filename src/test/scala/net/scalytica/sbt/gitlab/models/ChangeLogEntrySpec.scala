package net.scalytica.sbt.gitlab.models

import net.scalytica.sbt.gitlab.test.ModelFixtures
import org.scalatest.{MustMatchers, OptionValues, WordSpec}

// scalastyle:off magic.number
class ChangeLogEntrySpec
    extends WordSpec
    with MustMatchers
    with OptionValues
    with ModelFixtures {

  "The ChangeLogEntry" should {

    "return a change log entry with all commits for the first tag" in {
      val c      = commits(num = 33)
      val tagRef = c.drop(13).headOption.value
      val t = tag(
        name = "test-tag-foo",
        target = CommitRef(sha1("test-tag-A")),
        commit = tagRef,
        message = Some("Foo Tag")
      )

      val res = ChangeLogEntry.forTag(t, None, c)

      res.tag mustBe t
      res.commits mustBe c.take(14).reverse
    }

    "return a change log entry with all commits for tag after the first" in {
      val c       = commits(num = 33)
      val tagRefA = c.drop(13).headOption.value
      val tagRefB = c.drop(22).headOption.value
      val t1 = tag(
        name = "test-tag-A",
        target = CommitRef(sha1("test-tag-A")),
        commit = tagRefA,
        message = Some("Tag A")
      )
      val t2 = tag(
        name = "test-tag-B",
        target = CommitRef(sha1("test-tag-B")),
        commit = tagRefB,
        message = Some("Tag B")
      )

      val res = ChangeLogEntry.forTag(t2, Some(t1), c)

      res.tag mustBe t2
      res.commits mustBe c.slice(14, 23).reverse
    }

  }

}
