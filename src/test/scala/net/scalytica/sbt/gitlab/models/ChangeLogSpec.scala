package net.scalytica.sbt.gitlab.models

import net.scalytica.sbt.gitlab.test.ModelFixtures
import org.scalatest.Inspectors.forAll
import org.scalatest.{MustMatchers, OptionValues, WordSpec}

// scalastyle:off magic.number
class ChangeLogSpec
    extends WordSpec
    with MustMatchers
    with OptionValues
    with ModelFixtures {

  "A ChangeLog" should {

    "be initialised from a list of tags and commits" in {
      val (tags, commits) = tagsAndCommits()

      val res = ChangeLog(tags, commits)
      res.entries must have size 5

      forAll(res.entries.reverse.zipWithIndex) {
        case (entry, index) =>
          entry.commits must have size 3

          entry.tag.name mustBe s"test-tag-${index + 1}"
          entry.tag.commit mustBe entry.commits.headOption.value
          entry.tag.message mustBe Some(s"Test Tag ${index + 1}")

          forAll(entry.commits) { commit =>
            commit.title must startWith("test-commit-")
            commit.message.value must startWith("Test commit message")
          }
      }
    }

    "only include tags from specified starting point" in {
      val (tags, commits) = tagsAndCommits()

      val res = ChangeLog(tags, commits, Some("test-tag-3"))
      res.entries must have size 3

      forAll(res.entries.reverse.zipWithIndex) {
        case (entry, index) =>
          entry.commits must have size 3

          entry.tag.name mustBe s"test-tag-${index + 3}"
          entry.tag.commit mustBe entry.commits.headOption.value
          entry.tag.message mustBe Some(s"Test Tag ${index + 3}")

          forAll(entry.commits) { commit =>
            commit.title must startWith("test-commit-")
            commit.message.value must startWith("Test commit message")
          }
      }
    }

  }

}
