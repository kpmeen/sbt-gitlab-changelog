package net.scalytica.sbt.gitlab.api

import net.scalytica.sbt.gitlab.models.{Commit, Issue, Milestone, Tag}
import net.scalytica.sbt.gitlab.test.IntegrationConfigs
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpec}

class GitLabClientSpec
    extends WordSpec
    with BeforeAndAfterAll
    with MustMatchers
    with IntegrationConfigs {

  val client = GitLabClient(host, token, ProjectUri("kpmeen", "symbiotic"))

  override def afterAll(): Unit = {
    client.close.unsafeRunSync()
    super.afterAll()
  }

  "The GitLab client" should {

    "fetch a list of all commits from GitLab" in {
      val res: Seq[Commit] = client.getCommits.unsafeRunSync()
      res must have size 689
      // TODO: Add more detailed assertions of response
    }

    "fetch a list of all tags from GitLab" in {
      val res: Seq[Tag] = client.getTags.unsafeRunSync()
      res must have size 18
      // TODO: Add more detailed assertions of response
    }

    "fetch a list of all issues from GitLab" in {
      val res: Seq[Issue] = client.getIssues.unsafeRunSync()
      res must not be empty
      // TODO: Add more detailed assertions of response
    }

    "fetch a list of all milestones from GitLab" in {
      val res: Seq[Milestone] = client.getMilestones.unsafeRunSync()
      res must not be empty
      // TODO: Add more detailed assertions of response
    }

  }

}
