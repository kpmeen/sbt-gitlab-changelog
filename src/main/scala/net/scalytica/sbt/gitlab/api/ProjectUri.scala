package net.scalytica.sbt.gitlab.api

import java.net.URLEncoder

case class ProjectUri(namespace: String, projectName: String) {

  val urlEncoded: String =
    URLEncoder.encode(s"$namespace/$projectName", "UTF-8")

}
