package net.scalytica.sbt.gitlab.api

object APIVersions {

  sealed abstract class APIVersion(val version: String) {
    override def toString = version
  }

  case object V4 extends APIVersion("v4")

}
