package net.scalytica.sbt.gitlab.api

import java.time.ZonedDateTime

import cats.effect.IO
import cats.syntax.either._
import io.circe._
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.jackson._
import net.scalytica.sbt.gitlab.api.APIVersions.APIVersion
import net.scalytica.sbt.gitlab.models._
import org.http4s.client.blaze._
import org.http4s.util.CaseInsensitiveString
import org.http4s.{Header, Request, Response, Uri}
import org.slf4j.LoggerFactory

import scala.concurrent.duration._

class GitLabClient private (
    gitlabHost: GitLabHost,
    accessToken: AccessToken,
    projectUri: ProjectUri,
    apiVersion: APIVersion
) {

  private[this] val logger = LoggerFactory.getLogger(classOf[GitLabClient])

  object Urls {
    val MaxResultsPerPage = "100"

    lazy val BaseUrl = GitLabUrl(value = s"${gitlabHost.url}/api/$apiVersion")

    lazy val BaseProjUrl = BaseUrl.append(s"/projects/${projectUri.urlEncoded}")

    lazy val TagsUrl = BaseProjUrl
      .append(s"/repository/tags")
      .withQueryParams("per_page" -> MaxResultsPerPage)

    lazy val CommitsUrl = BaseProjUrl
      .append("/repository/commits")
      .withQueryParams(
        "ref_name" -> "master",
        "per_page" -> MaxResultsPerPage
      )

    lazy val IssuesUrl = BaseProjUrl
      .append(s"/issues")
      .withQueryParams(
        "state"    -> "closed",
        "per_page" -> MaxResultsPerPage
      )

    lazy val MilestonesUrl = BaseProjUrl
      .append(s"/milestones")
      .withQueryParams(
        "state"    -> "closed",
        "per_page" -> MaxResultsPerPage
      )
  }

  private[this] val AppJsonHeaderValue = "application/json"
  private[this] val TokenHeaderKey     = "PRIVATE-TOKEN"

  private[this] val ReqHeaders = Seq(
    Header("Accept", AppJsonHeaderValue),
    Header("Content-Type", AppJsonHeaderValue),
    Header(TokenHeaderKey, accessToken.value)
  )

  private[this] val http4sClient =
    Http1Client[IO](
      BlazeClientConfig.defaultConfig.copy(
        requestTimeout = 5 minutes,
        responseHeaderTimeout = 5 minutes
      )
    )

  implicit val snakeCaseConfig =
    Configuration.default.withSnakeCaseMemberNames

  implicit val encodeInstant: Encoder[ZonedDateTime] =
    Encoder.encodeString.contramap[ZonedDateTime](_.toString)

  implicit val decodeInstant: Decoder[ZonedDateTime] =
    Decoder.decodeString.emap { str =>
      Either
        .catchNonFatal(ZonedDateTime.parse(str))
        .leftMap(t => s"ZonedDateTime could not be parsed: ${t.getMessage}")
    }

  private[this] val LinkHeaderKey = CaseInsensitiveString("Link")

  private[this] def nextUrl(response: Response[IO]): Option[GitLabUrl] =
    parseLinkHeader(response).get("next").map(GitLabUrl.apply)

  private[this] def parseLinkHeader(resp: Response[IO]): Map[String, String] = {
    resp.headers
      .get(LinkHeaderKey)
      .map { header =>
        val linkLines = header.value.split(",").map(_.trim)
        linkLines.map { ll =>
          val splat   = ll.splitAt(ll.indexOf(";"))
          val linkStr = splat._1.stripPrefix("<").stripSuffix(">")
          val key     = splat._2.stripPrefix("""; rel="""").stripSuffix("\"")
          key -> linkStr
        }.toMap
      }
      .getOrElse(Map.empty)
  }

  private[this] def request(gitlabUrl: GitLabUrl): Request[IO] = {
    logger.trace(s"Building request with URL: ${gitlabUrl.url}")
    Request[IO](uri = Uri.unsafeFromString(gitlabUrl.url))
      .putHeaders(ReqHeaders: _*)
  }

  /**
   *
   * @param gitlabUrl
   * @param decoder
   * @tparam T
   * @return
   */
  private[this] def getList[T](
      gitlabUrl: GitLabUrl
  )(implicit decoder: Decoder[Seq[T]]): IO[Seq[T]] = {
    def next(
        optLink: Option[GitLabUrl],
        stateIO: IO[Seq[T]] = IO.pure(Seq.empty[T])
    ): IO[Seq[T]] = optLink match {
      case None =>
        logger.debug("No more pages...processing complete")
        stateIO

      case Some(link) =>
        http4sClient.flatMap { client =>
          client.fetch(request(link)) { response =>
            val resIO =
              response
                .as[String]
                .flatMap(js => IO.fromEither(decode[Seq[T]](js)))
                .flatMap(t => stateIO.map(_ ++: t))

            val nextUrlLink = nextUrl(response)
            logger.trace(s"next url is: $nextUrlLink")
            next(nextUrlLink, resIO)
          }
        }
    }

    next(Some(gitlabUrl))
  }

  def close: IO[Unit] = http4sClient.flatMap(_.shutdown)

  // TODO: Haven't decided if this is necessary
  def getMilestones: IO[Seq[Milestone]] = {
    implicit val decoder: Decoder[Seq[Milestone]] = Decoder.decodeSeq[Milestone]

    getList[Milestone](Urls.MilestonesUrl)
  }

  // TODO: get issues resolved after commit ref for tag
  def getIssues: IO[Seq[Issue]] = {
    implicit val decoder: Decoder[Seq[Issue]] = Decoder.decodeSeq[Issue]

    getList[Issue](Urls.IssuesUrl)
  }

  /**
   * Get all tags for [[projectUri]] from the GitLab commits API.
   *
   * @return a collection of Tag data
   */
  def getTags: IO[Seq[Tag]] = {
    implicit val decoder: Decoder[Seq[Tag]] = Decoder.decodeSeq[Tag]

    getList[Tag](Urls.TagsUrl)
  }

  /**
   * Retrieve all commits for [[projectUri]] from the GitLab commits API.
   *
   * @return a collection of Commit data
   */
  def getCommits: IO[Seq[Commit]] = {
    implicit val decoder: Decoder[Seq[Commit]] = Decoder.decodeSeq[Commit]

    getList[Commit](Urls.CommitsUrl)
  }

}

object GitLabClient {

  def apply(
      gitlabHost: GitLabHost,
      accessToken: AccessToken,
      projectPath: ProjectUri,
      apiVersion: APIVersion = APIVersions.V4
  ): GitLabClient =
    new GitLabClient(gitlabHost, accessToken, projectPath, apiVersion)

}
