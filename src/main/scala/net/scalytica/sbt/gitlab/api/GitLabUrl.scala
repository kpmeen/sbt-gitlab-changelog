package net.scalytica.sbt.gitlab.api

case class GitLabUrl(
    value: String,
    queryParams: Map[String, String] = Map.empty
) {
  def withPage(pageNum: Int): GitLabUrl = GitLabUrl(s"$value&page=$pageNum")
  def append(path: String): GitLabUrl = {
    copy(s"${value.stripSuffix("/")}/${path.stripPrefix("/")}")
  }
  def withQueryParams(params: (String, String)*): GitLabUrl =
    copy(queryParams = params.toMap)

  def url: String =
    s"$value?${queryParams.map(kv => s"${kv._1}=${kv._2}").mkString("&")}"

}

object GitLabUrl {
  def apply(str: String): GitLabUrl = {
    val (url, queryStr) = str.splitAt(str.indexOf("?") + 1)
    val params = queryStr
      .split("&")
      .map(s => s.splitAt(s.indexOf("=")))
      .toMap
      .mapValues(_.stripPrefix("="))

    GitLabUrl(url.stripSuffix("?"), params)
  }
}
