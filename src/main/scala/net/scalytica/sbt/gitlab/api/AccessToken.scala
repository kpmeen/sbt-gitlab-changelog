package net.scalytica.sbt.gitlab.api

case class AccessToken(value: String) {
  require(Option(value).nonEmpty)
}
