package net.scalytica.sbt

import java.time.{ZoneId, ZonedDateTime}

import scala.math.Ordering

package object gitlab {

  object OrderingImplicits {
    implicit val sortedZonedDateTimeAscSeq: Ordering[ZonedDateTime] =
      Ordering.by { zdt: ZonedDateTime =>
        zdt.toInstant.toEpochMilli
      }
  }

  /**
   * ZonedDateTime used to indicate an empty date. There's not much chance of a
   * commit having been done in year 0. (ಠ‿↼)
   */
  val EmptyDate: ZonedDateTime =
    ZonedDateTime.of(0, 1, 1, 0, 0, 0, 0, ZoneId.of("UTC"))

}
