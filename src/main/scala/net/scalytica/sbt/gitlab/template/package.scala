package net.scalytica.sbt.gitlab

package object template {

  private[template] val defaultTemplateName = "/changelog.beard"

  private[template] val DefaultFooterValue =
    "Created using [sbt-gitlab-changelog]" +
      "(https://gitlab.com/kpmeen/sbt-gitlab-changelog)."

}
