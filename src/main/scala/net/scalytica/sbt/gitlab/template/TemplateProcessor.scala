package net.scalytica.sbt.gitlab.template

import java.nio.file.{Path, Paths}

import cats.effect.IO
import de.zalando.beard.renderer._
import net.scalytica.sbt.gitlab.api.{GitLabHost, ProjectUri}
import net.scalytica.sbt.gitlab.models.{ChangeLog, CommitRef}

class TemplateProcessor(
    projectUri: ProjectUri,
    gitlabHost: GitLabHost = GitLabHost(),
    changeLogPath: Path = Paths.get("CHANGELOG.md"),
    templatePath: Option[Path] = None
) extends TemplateContextKeys {

  /** Type alias for the template Context */
  private[this] type Context = Map[String, Any]

  private[this] val compiler = DefaultTemplateCompiler
  private[this] val templateName = TemplateName(
    templatePath.map(_.toAbsolutePath.toString).getOrElse(defaultTemplateName)
  )

  /**
   *
   * @param changelog
   * @return
   */
  def renderChangelogFile(changelog: ChangeLog): IO[Unit] = {
    val renderResult = FileWriterRenderResult(changeLogPath)
    renderWith(changelog, renderResult).map { fw =>
      fw.flush()
      fw.close()
    }
  }

  /**
   *
   * @param changelog
   * @return
   */
  def renderChangelogString(changelog: ChangeLog): IO[String] = {
    val renderResult = StringWriterRenderResult()
    renderWith(changelog, renderResult).map(_ => renderResult.result.toString)
  }

  /**
   *
   * @param changelog
   * @param renderResult
   * @tparam T
   * @return
   */
  private[this] def renderWith[T](
      changelog: ChangeLog,
      renderResult: RenderResult[T]
  ): IO[T] = {
    val templateTry  = compiler.compile(templateName)
    val renderer     = new BeardTemplateRenderer(compiler)
    val ctx: Context = changeLogContext(changelog)

    IO.fromEither {
      templateTry
        .map(template => renderer.render(template, renderResult, ctx))
        .toEither
    }
  }

  /**
   *
   * @param changelog
   * @return
   */
  def changeLogContext(changelog: ChangeLog): Context = {
    def commitLink(ref: CommitRef): String = {
      s"${gitlabHost.url}/${projectUri.namespace}/${projectUri.projectName}" +
        s"/commit/${ref.value}"
    }

    Map(
      TitleKey -> s"Changelog for ${projectUri.projectName}",
      ChangeLogEntriesKey -> changelog.entries.map { entry =>
        Map(
          TagKey -> Map(
            NameKey -> entry.tag.name
          ),
          CommitsKey -> entry.commits.map { commit =>
            Map(
              ShortRefKey -> commit.shortId.value,
              TitleKey    -> commit.title,
              MessageKey  -> commit.message,
              LinkKey     -> commitLink(commit.id)
            )
          }
        )
      },
      FooterKey -> DefaultFooterValue
    )
  }

}
