package net.scalytica.sbt.gitlab.template

trait TemplateContextKeys {

  val TitleKey: String            = "title"
  val ChangeLogEntriesKey: String = "changeLogEntries"
  val TagKey: String              = "tag"
  val NameKey: String             = "name"
  val CommitsKey: String          = "commits"
  val ShortRefKey: String         = "shortRef"
  val MessageKey: String          = "message"
  val FooterKey: String           = "footer"
  val LinkKey: String             = "link"

}

object TemplateContextKeys extends TemplateContextKeys
