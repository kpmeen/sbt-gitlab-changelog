package net.scalytica.sbt.gitlab.template

import java.io.FileWriter
import java.nio.file.{Files, Path}

import de.zalando.beard.renderer.RenderResult

case class FileWriterRenderResult(filePath: Path)
    extends RenderResult[FileWriter] {

  val file = {
    Files.deleteIfExists(filePath)
    filePath.toFile
  }
  val fileWriter = new FileWriter(file)

  override def write(renderedChunk: String): Unit = {
    fileWriter.write(renderedChunk)
  }

  override def result = fileWriter

}
