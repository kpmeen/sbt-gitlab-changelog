package net.scalytica.sbt.gitlab.changelog

import java.nio.file.Path

import cats.effect.IO
import net.scalytica.sbt.gitlab.api.APIVersions.APIVersion
import net.scalytica.sbt.gitlab.api._
import net.scalytica.sbt.gitlab.models.ChangeLog
import net.scalytica.sbt.gitlab.template.TemplateProcessor

class GitLabChangeLogProcessor(
    gitlabHost: GitLabHost,
    accessToken: AccessToken,
    projectPath: ProjectUri,
    destPath: Path,
    startAtTag: Option[String],
    apiVersion: APIVersion
) {

  lazy val client =
    GitLabClient(gitlabHost, accessToken, projectPath, apiVersion)

  lazy val templateProcessor = new TemplateProcessor(
    projectUri = projectPath,
    gitlabHost = gitlabHost,
    changeLogPath = destPath
  )

  def generateChangeLog: IO[Unit] =
    changeLog.flatMap(data => templateProcessor.renderChangelogFile(data))

  private[changelog] def changeLog: IO[ChangeLog] = {
    for {
      tags    <- client.getTags
      commits <- client.getCommits
    } yield ChangeLog(tags, commits, startAtTag)
  }
}

object GitLabChangeLogProcessor {

  def apply(
      gitlabHost: GitLabHost,
      accessToken: AccessToken,
      projectPath: ProjectUri,
      destPath: Path,
      startAtTag: Option[String] = None,
      apiVersion: APIVersion = APIVersions.V4
  ): GitLabChangeLogProcessor =
    new GitLabChangeLogProcessor(
      gitlabHost,
      accessToken,
      projectPath,
      destPath,
      startAtTag,
      apiVersion
    )

}
