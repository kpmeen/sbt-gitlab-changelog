package net.scalytica.sbt.gitlab.changelog

import java.nio.file.{Path, Paths}

import net.scalytica.sbt.gitlab.api.{AccessToken, GitLabHost, ProjectUri}
import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin.autoImport._
import sbtrelease.{ReleasePlugin, Vcs}

import scala.language.postfixOps
import scala.sys.process.ProcessLogger
import scala.util.Properties

object GitLabChangeLogPlugin extends AutoPlugin {

  override def trigger: PluginTrigger = allRequirements
  override def requires: Plugins      = ReleasePlugin

  object autoImport {

    lazy val gitlabUseTLS: SettingKey[Boolean] =
      settingKey[Boolean](
        "Use HTTP or HTTPS with the GitLab API. Defaults to true."
      )

    lazy val gitlabProjectNamespace: SettingKey[String] =
      settingKey[String](
        "The GitLab namespace for this project. The namespace is typically " +
          "the name of the repos owner."
      )

    lazy val gitlabProjectName: SettingKey[String] =
      settingKey[String](
        "The GitLab repository name. Defaults to this sbt project name."
      )

    lazy val gitlabHost: SettingKey[String] =
      settingKey[String]("GitLab hostname. Defaults to gitlab.com.")

    lazy val gitlabToken: SettingKey[String] =
      settingKey[String]("GitLab API token to use.")

    lazy val changelogFile: SettingKey[Path] =
      settingKey[Path](
        "The path of the file to create when generating the changelog." +
          " Defaults to 'CHANGELOG.md' in the project root directory."
      )
    lazy val changeLogStartAtTagName: SettingKey[Option[String]] =
      settingKey[Option[String]](
        "Tells the sbt-gitlab-changelog plugin which git tag to start at when" +
          " generating the changelog file. The value must match the tag name " +
          "exactly. Defaults to None which includes all tags (and commits)."
      )

    lazy val generateChangelog: TaskKey[Unit] =
      taskKey[Unit]("Generates the CHANGELOG.md file to upload to GitLab.")

    lazy val generateReleaseChangelog = ReleaseStep(generateChangelogStep)

    lazy val commitChangelog = ReleaseStep(commitChangelogStep)
  }

  import autoImport._

  // scalastyle:off method.length
  override def projectSettings: Seq[Setting[_]] = {
    Seq(
      gitlabToken := {
        Properties
          .envOrNone("GITLAB_API_TOKEN")
          .orElse(Properties.propOrNone("GITLAB_API_TOKEN"))
          .getOrElse {
            val host = GitLabHost(gitlabHost.value, gitlabUseTLS.value)
            // scalastyle:off
            println(
              "The GitLab access token needs to be defined. Please see " +
                s"${host.url}/profile/personal_access_tokens for more" +
                s" details about how to create one. Then set the " +
                s"${gitlabToken.key.label} or the 'GITLAB_API_TOKEN' system " +
                s"environment property"
            )
            // scalastyle:on
            ""
          }
      },
      gitlabUseTLS := true,
      gitlabHost := "gitlab.com",
      gitlabProjectName := name.value,
      changelogFile := {
        val defaultPath = baseDirectory.value.getAbsolutePath + "/CHANGELOG.md"
        Paths.get(defaultPath)
      },
      changeLogStartAtTagName := None,
      generateChangelog := {
        val log   = streams.value.log
        val token = AccessToken(gitlabToken.value)
        val host  = GitLabHost(gitlabHost.value, gitlabUseTLS.value)

        val changeLogProc = GitLabChangeLogProcessor(
          gitlabHost = host,
          accessToken = token,
          projectPath = ProjectUri(
            namespace = gitlabProjectNamespace.value,
            projectName = gitlabProjectName.value
          ),
          destPath = changelogFile.value,
          startAtTag = changeLogStartAtTagName.value
        )

        import scala.concurrent.duration._
        changeLogProc.generateChangeLog.unsafeRunTimed(2 minutes) match {
          case Some(_) =>
            log.info(
              s"${changelogFile.value.getFileName} generated in" +
                s" ${changelogFile.value.toAbsolutePath}"
            )
          case None =>
            log.err(s"${changelogFile.value.getFileName} could not be created.")
        }
      }
    )
  }
  // scalastyle:on method.length

  private[this] def generateChangelogStep(state: State): State = {
    Project.extract(state).runTask(generateChangelog, state)._1
  }

  private[this] def commitChangelogStep(state: State): State = {
    val log           = toProcessLogger(state)
    val base          = vcs(state).baseDir
    val sign          = Project.extract(state).get(releaseVcsSign)
    val changelogFile = base / "CHANGELOG.md"

    val relativePath = IO
      .relativize(base, changelogFile)
      .getOrElse(
        "Version file [CHANGELOG.md] is not in the repository with base " +
          s"directory [${base.getAbsolutePath}]!"
      )

    vcs(state).add(relativePath) !! log

    val vcsAddOutput = (vcs(state).status !!).trim
    if (vcsAddOutput.isEmpty) state.log.info("CHANGELOG.md has not changed.")
    else vcs(state).commit("Update changelog", sign) ! log

    state
  }

  /**
   * Extracts the used vcs.
   *
   * Copied from the sbt-release plugin.
   *
   * @param state sbt state
   * @return vcs implementation
   */
  private[this] def vcs(state: State): Vcs =
    Project
      .extract(state)
      .get(releaseVcs)
      .getOrElse(
        sys.error(
          "Aborting release. Working directory is not a repository " +
            "of a recognized VCS."
        )
      )

  /**
   * Creates a ProcessLogger from the current sbt state.
   *
   * Copied from the sbt-release plugin.
   *
   * @param state the sbt State
   * @return a process logger
   */
  private[this] def toProcessLogger(state: State): ProcessLogger =
    new ProcessLogger {
      override def err(s: => String): Unit = state.log.info(s)
      override def out(s: => String): Unit = state.log.info(s)
      override def buffer[T](f: => T): T   = state.log.buffer(f)
    }
}
