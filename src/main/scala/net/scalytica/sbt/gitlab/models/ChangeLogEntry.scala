package net.scalytica.sbt.gitlab.models

case class ChangeLogEntry(tag: Tag, commits: Seq[Commit])

object ChangeLogEntry {

  val empty = ChangeLogEntry(Tag.empty, Seq.empty)

  def forTag(
      tag: Tag,
      maybeFromTag: Option[Tag],
      commits: Seq[Commit]
  ): ChangeLogEntry = {
    val endIdx   = indexOfTag(tag, commits)
    val startIdx = maybeFromTag.map(t => indexOfTag(t, commits)).getOrElse(0)
    val changes  = commits.slice(startIdx, endIdx).reverse
    ChangeLogEntry(tag, changes)
  }

  private[this] def indexOfTag(tag: Tag, commits: Seq[Commit]): Int =
    commits.indexWhere(_.id == tag.commit.id) + 1
}
