package net.scalytica.sbt.gitlab.models

import java.time.ZonedDateTime

case class Issue(
    id: Int,
    iid: Int,
    projectId: Int,
    title: String,
    state: ClosedOrActive,
    webUrl: String,
    createdAt: ZonedDateTime,
    userNotesCount: Option[Int],
    confidential: Option[Boolean],
    discussionLocked: Option[Boolean],
    description: Option[String],
    labels: Option[Seq[String]],
    weight: Option[Int],
    updatedAt: Option[ZonedDateTime],
    dueDate: Option[ZonedDateTime],
    closedAt: Option[ZonedDateTime],
    closedBy: Option[SimpleUser],
    milestone: Option[Milestone],
    author: Option[SimpleUser],
    assignees: Option[Seq[SimpleUser]],
    assignee: Option[SimpleUser],
    timeStats: Option[TimeStats]
)

/*
  TODO: Supported durations for human*:
    - months (mo)
    - weeks (w)
    - days (d)
    - hours (h)
    - minutes (m)
 */

case class TimeStats(
    timeEstimate: Int, // In seconds
    totalTimeSpent: Int, // In Seconds
    humanTimeEstimate: Option[String], // FIXME: Data type???
    humanTotalTimeSpent: Option[String] // FIXME: Data type???
)
