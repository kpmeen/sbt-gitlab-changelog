package net.scalytica.sbt.gitlab.models

case class Milestone(
    id: Int,
    iid: Int,
    projectId: Int,
    title: String,
    state: ClosedOrActive
)
