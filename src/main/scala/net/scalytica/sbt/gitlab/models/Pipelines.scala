package net.scalytica.sbt.gitlab.models

import io.circe.{Decoder, Encoder, Json}

sealed trait PipelineStatus
case object Running  extends PipelineStatus
case object Pending  extends PipelineStatus
case object Success  extends PipelineStatus
case object Failed   extends PipelineStatus
case object Canceled extends PipelineStatus
case object Skipped  extends PipelineStatus

object PipelineStatus {

  implicit val encodePipelineStatus: Encoder[PipelineStatus] =
    Encoder.instance {
      case Running  => Json.fromString("running")
      case Pending  => Json.fromString("pending")
      case Success  => Json.fromString("success")
      case Failed   => Json.fromString("failed")
      case Canceled => Json.fromString("canceled")
      case Skipped  => Json.fromString("skipped")
    }

  implicit val decodePipelineStatus: Decoder[PipelineStatus] =
    Decoder.decodeString.emap {
      case str if str == "running"  => Right(Running)
      case str if str == "pending"  => Right(Pending)
      case str if str == "success"  => Right(Success)
      case str if str == "failed"   => Right(Failed)
      case str if str == "canceled" => Right(Canceled)
      case str if str == "skipped"  => Right(Skipped)
      case str                      => Left(s"$str is not a known state")
    }

}

case class SimplePipeline(
    id: Int,
    ref: String,
    sha: CommitRef,
    status: PipelineStatus
)
