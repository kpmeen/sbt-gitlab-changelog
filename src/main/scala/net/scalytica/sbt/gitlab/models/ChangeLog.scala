package net.scalytica.sbt.gitlab.models

import net.scalytica.sbt.gitlab.OrderingImplicits._

case class ChangeLog(entries: Seq[ChangeLogEntry])

object ChangeLog {

  def apply(
      tags: Seq[Tag],
      commits: Seq[Commit],
      startAtTag: Option[String] = None
  ): ChangeLog = {
    val sortedCommits = commits.sortBy(_.createdAt)
    val sortedTags    = tags.sortBy(_.commit.createdAt)

    val entries = (Tag.empty +: sortedTags)
      .sliding(size = 2, step = 1)
      .map {
        case Tag.empty :: firstTag :: Nil =>
          ChangeLogEntry.forTag(firstTag, None, sortedCommits)

        case prevTag :: currTag :: Nil =>
          ChangeLogEntry.forTag(currTag, Option(prevTag), sortedCommits)

        case _ =>
          ChangeLogEntry.empty
      }
      .toSeq
      .filterNot(_ == ChangeLogEntry.empty)

    // Only include entries starting from startAtTag argument if it has a value
    val includeEntries = startAtTag.map { sat =>
      if (entries.nonEmpty) {
        val i = entries.indexWhere(_.tag.name == sat)
        entries.slice(i, entries.length)
      } else entries
    }.getOrElse(entries).reverse

    ChangeLog(includeEntries)
  }

}
