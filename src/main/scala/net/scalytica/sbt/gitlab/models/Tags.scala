package net.scalytica.sbt.gitlab.models

case class Release(tagName: String, description: Option[String])

object Release {
  val empty = Release("", None)
}

case class Tag(
    name: String,
    target: CommitRef,
    commit: Commit,
    message: Option[String] = None,
    release: Option[Release] = None
)

object Tag {
  val empty = Tag(
    name = "",
    target = CommitRef.empty,
    commit = Commit.empty
  )
}
