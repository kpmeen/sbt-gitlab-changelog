package net.scalytica.sbt.gitlab.models

import java.time.ZonedDateTime

import io.circe.{Decoder, Encoder, Json}
import net.scalytica.sbt.gitlab.EmptyDate

case class CommitRef(value: String) {
  require(value.length == 40)
}

object CommitRef {
  val empty = CommitRef("0000000000000000000000000000000000000000")

  implicit val encodeCommitRef: Encoder[CommitRef] =
    Encoder.instance(v => Json.fromString(v.value))

  implicit val decodeCommitRef: Decoder[CommitRef] =
    Decoder.decodeString.emap(s => Right(CommitRef(s)))
}

case class CommitShortRef(value: String) extends AnyVal

object CommitShortRef {
  val empty = CommitShortRef("00000000000")

  implicit val encodeCommitShortRef: Encoder[CommitShortRef] =
    Encoder.instance(v => Json.fromString(v.value))

  implicit val decodeCommitShortRef: Decoder[CommitShortRef] =
    Decoder.decodeString.emap(s => Right(CommitShortRef(s)))
}

case class CommitStats(
    additions: Option[Int] = None,
    deletions: Option[Int] = None,
    total: Option[Int] = None
)

case class Commit(
    id: CommitRef,
    shortId: CommitShortRef,
    title: String,
    createdAt: ZonedDateTime,
    message: Option[String] = None,
    committerName: Option[String] = None,
    committerEmail: Option[String] = None,
    committedDate: Option[ZonedDateTime] = None,
    authorName: Option[String] = None,
    authorEmail: Option[String] = None,
    authoredDate: Option[ZonedDateTime] = None,
    parentIds: Option[List[CommitRef]] = None,
    lastPipeline: Option[PipelineStatus] = None,
    stats: Option[CommitStats] = None
)

object Commit {
  val empty = Commit(
    id = CommitRef.empty,
    shortId = CommitShortRef.empty,
    title = "",
    createdAt = EmptyDate
  )
}
