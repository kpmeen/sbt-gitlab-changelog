package simple

import net.scalytica.foo.Calculate

/**
  * A simple class and objects to write tests against.
  */
class Main {
  val calc = new Calculate(Main.constant)
  val default = "the function returned"
  def method = default + " " + calc.square
}

object Main {

  val constant = 1

  def main(args: Array[String]): Unit = {
    println(new Main().method)
  }
}
