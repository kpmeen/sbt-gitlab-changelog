version := "0.1"
scalaVersion := "2.12.6"

lazy val root = (project in file("."))
  .enablePlugins(GitLabChangeLogPlugin)
  .settings(
    gitlabProjectNamespace := "kpmeen",
    gitlabProjectName := "clammyscan",
    changeLogStartAtTagName := Some("v1.1.0")
  )
  .aggregate(foo, bar)
lazy val foo = (project in file("foo")).disablePlugins(GitLabChangeLogPlugin)
lazy val bar =
  (project in file("bar")).disablePlugins(GitLabChangeLogPlugin).dependsOn(foo)
