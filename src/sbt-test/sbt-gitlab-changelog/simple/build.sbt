version := "0.1"
scalaVersion := "2.12.6"

gitlabProjectNamespace := "kpmeen"
gitlabProjectName := "clammyscan"
changeLogStartAtTagName := Some("v1.1.0")