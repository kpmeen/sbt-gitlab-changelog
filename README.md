[![pipeline status](https://gitlab.com/kpmeen/sbt-gitlab-changelog/badges/master/pipeline.svg)](https://gitlab.com/kpmeen/sbt-gitlab-changelog/commits/master)
[![Download](https://api.bintray.com/packages/kpmeen/sbt-plugins/sbt-gitlab-changelog/images/download.svg)](https://bintray.com/kpmeen/sbt-plugins/sbt-gitlab-changelog/_latestVersion)

# sbt-gitlab-changelog

An sbt AutoPlugin for generating change logs for repositories hosted on GitLab.

## Usage

This plugin requires sbt 1.x


### Enable plugin

To enable the plugin for you project add the following to your `project/plugins.sbt` file:

```scala
// Bintray resolver for the sbt-gitlab-changelog plugin
resolvers += Resolver.bintrayRepo("kpmeen", "sbt-plugins")
// Resolver for the Zalando bintray repository to access their Beard templating engine.
resolvers += Resolver.bintrayRepo("zalando", "maven")

// The sbt-gitlab-changelog plugin has a requirement to the sbt-release plugin.
// So it might be a good idea to enable that as well.
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.9")

// Add the sbt-gitlab-changelog plugin
addSbtPlugin("net.scalytica" % "sbt-gitlab-changelog" % "0.1.2")
```



### Testing

Run `test` for regular unit tests.

Run `scripted` for [sbt script tests](http://www.scala-sbt.org/1.x/docs/Testing-sbt-plugins.html).

## Issues

[https://gitlab.com/kpmeen/sbt-gitlab-changelog/issues](https://gitlab.com/kpmeen/sbt-gitlab-changelog/issues)


---
* [request inclusion in sbt-plugin-releases](https://bintray.com/sbt/sbt-plugin-releases)
* [Add your plugin to the community plugins list](https://github.com/sbt/website#attention-plugin-authors)
* [Claim your project an Scaladex](https://github.com/scalacenter/scaladex-contrib#claim-your-project)
