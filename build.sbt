// scalastyle:off
import sbt.Keys._
import sbt._
import sbtrelease.Vcs

import scala.sys.process.ProcessLogger

name := """sbt-gitlab-changelog"""
organization := "net.scalytica"
licenses += ("Apache-2.0", url("https://opensource.org/licenses/Apache-2.0"))

scalaVersion := "2.12.6"

sbtPlugin := true

scalacOptions := Seq(
  "-encoding",
  "utf-8", // Specify character encoding used by source files.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-unchecked", // Enable additional warnings where generated code depends on assumptions.
  "-explaintypes", // Explain type errors in more detail.
  "-Xfuture", // Turn on future language features.
  "-Xcheckinit", // Wrap field accessors to throw an exception on uninitialized access.
  "-Xfatal-warnings", // Fail the compilation if there are any warnings.
  "-Xlint:adapted-args", // Warn if an argument list is modified to match the receiver.
  "-Xlint:by-name-right-associative", // By-name parameter of right associative operator.
  "-Xlint:constant", // Evaluation of a constant arithmetic expression results in an error.
  "-Xlint:delayedinit-select", // Selecting member of DelayedInit.
  "-Xlint:doc-detached", // A Scaladoc comment appears to be detached from its element.
  "-Xlint:inaccessible", // Warn about inaccessible types in method signatures.
  "-Xlint:infer-any", // Warn when a type argument is inferred to be `Any`.
  "-Xlint:missing-interpolator", // A string literal appears to be missing an interpolator id.
  "-Xlint:nullary-override", // Warn when non-nullary `def f()' overrides nullary `def f'.
  "-Xlint:nullary-unit", // Warn when nullary methods return Unit.
  "-Xlint:option-implicit", // Option.apply used implicit view.
  "-Xlint:package-object-classes", // Class or object defined in package object.
  "-Xlint:poly-implicit-overload", // Parameterized overloaded implicit methods are not visible as view bounds.
  "-Xlint:private-shadow", // A private field (or class parameter) shadows a superclass field.
  "-Xlint:stars-align", // Pattern sequence wildcard must align with sequence component.
  "-Xlint:type-parameter-shadow", // A local type parameter shadows a type already in scope.
  "-Xlint:unsound-match", // Pattern match may not be typesafe.
  "-language:implicitConversions",
  "-language:experimental.macros", // Allow macro definition (besides implementation and application)
  "-language:higherKinds",
  "-language:existentials",
  "-language:postfixOps",
  "-Yno-adapted-args", // Do not adapt an argument list (either by inserting () or creating a tuple) to match the receiver.
  "-Ypartial-unification", // Enable partial unification in type constructor inference
  "-Ywarn-dead-code", // Warn when dead code is identified.
  "-Ywarn-extra-implicit", // Warn when more than one implicit parameter section is defined.
  "-Ywarn-numeric-widen", // Warn when numerics are widened.
  "-Ywarn-unused:implicits", // Warn if an implicit parameter is unused.
  "-Ywarn-unused:imports", // Warn if an import selector is not referenced.
  "-Ywarn-unused:locals", // Warn if a local definition is unused.
  "-Ywarn-unused:params", // Warn if a value parameter is unused.
  "-Ywarn-unused:patvars", // Warn if a variable bound in a pattern is unused.
  "-Ywarn-unused:privates" // Warn if a private member is unused.
)

scalacOptions in Test += "-Yrangepos"
logBuffered in Test := false
scriptedBufferLog := false

// Scalastyle
scalastyleFailOnWarning := true
scalastyleFailOnError := true

initialCommands in console := """import net.scalytica.sbt.gitlab._"""

// set up scripted sbt plugin for testing the sbt plugin
scriptedLaunchOpts ++= Seq(
  "-Xmx1024M",
  "-Dplugin.version=" + version.value
)

// Dependencies
resolvers ++= Seq(
  Resolver.defaultLocal,
  Resolver.mavenLocal,
  Resolver.sonatypeRepo("releases"),
  Resolver.bintrayRepo("zalando", "maven")
)

val scalatestVersion  = "3.0.5"
val circeVersion      = "0.9.3"
val http4sVersion     = "0.18.15"
val beardVersion      = "0.2.0"
val catsVersion       = "1.1.0"
val catsEffectVersion = "0.10.1"

// Zalando Beard templating engine
libraryDependencies ++= Seq(
  "de.zalando" %% "beard" % beardVersion
)

// Cats
libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-macros",
  "org.typelevel" %% "cats-kernel",
  "org.typelevel" %% "cats-core"
).map(_ % catsVersion)
libraryDependencies += "org.typelevel" %% "cats-effect" % catsEffectVersion

// Circe
libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-generic-extras"
).map(_ % circeVersion)
libraryDependencies += "io.circe" %% "circe-jackson29" % "0.9.0"

libraryDependencies += "org.spire-math" %% "jawn-parser" % "0.11.1"

// http4s
libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-blaze-client",
  "org.http4s" %% "http4s-dsl",
  "org.http4s" %% "http4s-circe"
).map(_ % http4sVersion)

// ScalaTest
libraryDependencies ++= Seq(
  "org.scalactic" %% "scalactic",
  "org.scalatest" %% "scalatest"
).map(_ % scalatestVersion % Test)

dependencyOverrides ++= Seq(
  "org.typelevel" % "cats-core_2.12"   % catsVersion,
  "org.typelevel" % "cats-effect_2.12" % catsEffectVersion
)

// plugin dependencies
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.9")

// ============ Release and publishing settings ============

// Bintray settings
bintrayPackageLabels := Seq("sbt", "plugin")
bintrayRepository := "sbt-plugins"
bintrayPackage := "sbt-gitlab-changelog"
bintrayVcsUrl := Some("""git@gitlab.com:kpmeen/sbt-gitlab-changelog.git""")
bintrayReleaseOnPublish := true

pomExtra :=
  <url>https://gitlab.com/kpmeen/sbt-gitlab-changelog</url>
    <scm>
      <url>git@gitlab.com:kpmeen/sbt-gitlab-changelog.git</url>
      <connection>scm:git:git@gitlab.com:kpmeen/sbt-gitlab-changelog.git</connection>
    </scm>
    <developers>
      <developer>
        <id>kpmeen</id>
        <name>Knut Petter Meen</name>
        <url>http://scalytica.net</url>
      </developer>
    </developers>
pomIncludeRepository := { _ =>
  false
}
autoAPIMappings := true

// Publishing
publishMavenStyle := true
publishArtifact in Test := false
publishArtifact in (Compile, packageSrc) := true
publishArtifact in (Compile, packageDoc) := false
publishArtifact in packageDoc := false
sources in (Compile, doc) := Seq.empty

def toProcessLogger(st: State): ProcessLogger = new ProcessLogger {
  override def err(s: => String): Unit = st.log.err(s)
  override def out(s: => String): Unit = st.log.info(s)
  override def buffer[T](f: => T): T   = st.log.buffer(f)
}

def vcs(state: State): Vcs =
  Project
    .extract(state)
    .get(releaseVcs)
    .getOrElse(
      sys.error(
        "Aborting release. Working directory is not a repository " +
          "of a recognized VCS."
      )
    )

def updateVersionInReadmeStep(state: State): State = {
  import scala.sys.process._
  import ReleasePlugin.autoImport._

  val log = toProcessLogger(state)

  val an = s""""sbt-gitlab-changelog""""
  state.get(ReleaseKeys.versions).foreach { v =>
    val findStr  = an + " % " + """"[0-9]*\.[0-9]\.[0-9]""""
    val replStr  = an + " % " + s""""${v._1}""""
    val regexStr = s"""s/$findStr/$replStr/g"""

    val OS = sys.props("os.name").toLowerCase
    log.out(s"Detected OS: $OS")

    val cmd = Seq.newBuilder[String]

    cmd += "sed"
    cmd += "-i"
    if (OS.indexOf("mac") >= 0) cmd += ""
    cmd += "-e"
    cmd += regexStr
    cmd += "README.md"

    cmd.result !
  }

  state
}

def commitReadme = { state: State =>
  val log  = toProcessLogger(state)
  val file = java.nio.file.Paths.get("README.md").toAbsolutePath.toFile
  val base = vcs(state).baseDir.getCanonicalFile
  val sign = Project.extract(state).get(releaseVcsSign)
  val relativePath = IO
    .relativize(base, file)
    .getOrElse(
      ("README.md file [%s] is outside of this VCS repository with " +
        "base directory [%s]!").format(file, base)
    )

  vcs(state).add(relativePath) !! log
  val status = (vcs(state).status !!) trim

  val newState = if (status.nonEmpty) {
    val msg = "Updated release version in README.md file"
    vcs(state).commit(msg, sign) ! log
    state
  } else {
    // nothing to commit. this happens if the README.md file hasn't changed.
    state
  }
  newState
}

lazy val updateReadmeStep = ReleaseStep(updateVersionInReadmeStep)
lazy val commitReadmeStep = ReleaseStep(commitReadme)

// Release process
import ReleaseTransformations._
releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,
  inquireVersions,
  runClean,
  runTest,
  releaseStepCommandAndRemaining("scripted"),
  setReleaseVersion,
  updateReadmeStep,
  commitReadmeStep,
  commitReleaseVersion,
  tagRelease,
  publishArtifacts,
  setNextVersion,
  commitNextVersion,
  pushChanges
)
